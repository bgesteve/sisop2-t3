DIR_OBJ=./obj/
DIR_SRC=./src/

AUX_NAME=auxiliar
AUX_OBJ=$(DIR_OBJ)$(AUX_NAME).o
AUX_SRC=$(DIR_SRC)$(AUX_NAME).c

HAS_NAME=tabelaHash
HAS_OBJ=$(DIR_OBJ)$(HAS_NAME).o
HAS_SRC=$(DIR_SRC)$(HAS_NAME).c

TER_NAME=terminal
TER_OBJ=$(DIR_OBJ)$(TER_NAME).o
TER_SRC=$(DIR_SRC)$(TER_NAME).c

CLI_NAME=client_tcp
CLI_OBJ=$(DIR_OBJ)$(CLI_NAME).o
CLI_SRC=$(DIR_SRC)$(CLI_NAME).c

SER_NAME=server_tcp
SER_OBJ=$(DIR_OBJ)$(SER_NAME).o
SER_SRC=$(DIR_SRC)$(SER_NAME).c



all: auxiliar tabelaHash terminal client server

auxiliar:
	gcc -o $(AUX_OBJ) -c $(AUX_SRC)

terminal:
	gcc -o $(TER_OBJ) -c $(TER_SRC)

tabelaHash:
	gcc -o $(HAS_OBJ) -c $(HAS_SRC)

client: 
	gcc -o $(CLI_OBJ) -c $(CLI_SRC)
	gcc -o $(CLI_NAME) $(AUX_OBJ) $(TER_OBJ) $(CLI_OBJ) -lpthread -Iinclude -lm


server: 
	gcc -o $(SER_OBJ) -c $(SER_SRC)
	gcc -o $(SER_NAME) $(AUX_OBJ) $(TER_OBJ) $(SER_OBJ) -lpthread -Iinclude -lm


clean:
	rm -rf $(DIR_OBJ)*.o
	rm -rf $(SER_NAME)
	rm -rf $(CLI_NAME)



/****************************************************
** UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
** INSITUTO DE INFORMÁTICA
** INFO1151 - SISTEMAS OPERACIONAIS II N (2016/1)
**
** Professor:
**     Alberto Egon Schaeffer Filho
**
**         COMUNICAÇÃO INTER-PROCESSOS USANDO SOCKETS UNIX
**
** Alunos:
**     Bruno Dias Freitas
**     Bruno Giovenardi Esteve
**     Erick Chemevski Espindula
**
** Descrição:
**
** Biblioteca auxiliar para tratar com a interface
** do chat.
**
** Para obter os efeitos desejados, foram utilizados
** ANSI_escape_code.
**
** Mais informaçoes em:
** https://en.wikipedia.org/wiki/ANSI_escape_code
**
****************************************************/

#define NOME_CHAT "LOBBY"
#define SEPARADOR       "==============================================="
#define SEPARADOR_NOME  "=================== LOBBY ====================="
#define SEPARADOR_MSG   "================== MENSAGENS =================="


int terminal_prepara_config();
void terminal_reseta_config();
int  terminal_get_tecla();
void terminal_apaga_linha(char *c, int p);
void terminal_apaga_linhas_anteriores(int linhas);
void terminal_get_linha(char *buffer, int tam);
void terminal_mensagem_padrao_chat(char *mensagem, char *nome_sala);
void terminal_escrever_mensagem_chat(char *mensagem, int eh_usuario, char *buffer, char *nome_sala);
void terminal_mensagem_inicial(char *nick);
void terminal_escolha_nick(char *nick, int tam);
int  terminal_escolher_comando(char *comando, int tam);
void terminal_mensagem_situacao(char *msg);
void terminal_lista_salas(int qtd_salas, int tam_nome, char salas[qtd_salas][tam_nome]);
int terminal_criar_sala(char *comando, int tam_max);
int terminal_entrar_sala(char *nome_sala, int tam_max, int nro_salas, char salas[][tam_max]);
int terminal_remover_sala(char *nome_sala, int tam_max, int nro_salas, char salas[][tam_max]);

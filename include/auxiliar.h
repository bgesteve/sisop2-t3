/****************************************************
** UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
** INSITUTO DE INFORMÁTICA
** INFO1151 - SISTEMAS OPERACIONAIS II N (2016/1)
**
** Professor:
**     Alberto Egon Schaeffer Filho
**
**         COMUNICAÇÃO INTER-PROCESSOS USANDO SOCKETS UNIX
**
** Alunos:
**     Bruno Dias Freitas
**     Bruno Giovenardi Esteve
**     Erick Chemevski Espindula
**
** Descrição:
**
** Biblioteca auxilir que contem as estruturas de dados
** utilizados pelo servidor, algumas funções de apoio e
** diversas constantes.
**
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>

#define PORT 35700

#define MAX_CLIENTS	10		//número máximo de clientes, usado no vetor de threads
#define MAX_SALAS 3

#define MAX_NOME_SALA 15
#define MAX_MSG 150			// tamanho máximo de uma mensagem do chat
#define MAX_NICK 15			//tamanho máximo de caracteres de um nickname
#define MAX_COMANDO 20      // Tamanho máximo de um comando
#define MAX_MSG_SERVIDOR 81
#define MAX_MSG_TEXTO 58
#define MAX_MSG_TOTAL MAX_MSG_TEXTO + MAX_NICK + 2 // Nick + " : " + Msg - \0 sobrando

#define SALA_INICIAL 0
#define NICK_PADRAO "Anonimo"

// comandos do chat:
#define CMD_LEAVE "\\leave"
#define CMD_JOIN "\\join"
#define CMD_CREATE "\\create"
#define CMD_CHANGENICK "\\nick"
#define CMD_EXIT "\\exit"
#define CMD_REMOVE "\\remove"
#define CMD_CANCEL "\\cancel"

#define NO_CMD 0
#define CMD_LEAVE_ID 1
#define CMD_JOIN_ID 2
#define CMD_CREATE_ID 3
#define CMD_WHERE_ID 4
#define CMD_NICK_ID 5
#define CMD_CANCEL_ID 6
#define CMD_EXIT_ID 7
#define CMD_REMOVE_ID 8


#define STATUS_CONEC "OK"
#define STATUS_ERRO "ERRO"

#define TRUE 1
#define FALSE 0
typedef int boolean;

typedef struct {
  pthread_t thread_id;
  char nick[MAX_NICK];
  int sala;
  int socket;
  int id;
}Cliente;

typedef struct {
	int id;
	char nome[MAX_NOME_SALA];
    boolean ocupado;
}Sala;

void aux_remove_quebra(char *msg_nova,char *msg_original);
int aux_compara_comandos(char *comando);
int aux_envia_msg_socket(int socket_dest, char *msg, int tam_msg);
int aux_recebe_msg_socket(int socket_orig, char *msg, int tam_msg);
void aux_apaga_sala(Sala *sala);
void aux_inicia_salas(int qtd_sala, Sala sala[]);
void aux_apaga_cliente(Cliente *cliente);
void aux_inicia_clientes(int qtd_cliente, Cliente clientes[]);

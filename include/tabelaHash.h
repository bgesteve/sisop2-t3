#define tam 677
#include "../include/auxiliar.h"

/*hash.h armazena a estrutura e os prototipos das funcaoes*/
struct dados {
    int info;
    Sala sala;
    struct dados *prox;
};

typedef struct dados Dados;
typedef Dados* Hash[tam];

int funcaoHash(int num);
void inicializaHash(Hash tab);
void insereHash(Hash tab, int num, Sala sala);
Sala buscaHash(Hash tab, int num);
void imprimeHash(Hash tab);
void removeHash(Hash tab, int num);
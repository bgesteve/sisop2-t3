/****************************************************
** UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
** INSITUTO DE INFORMÁTICA
** INFO1151 - SISTEMAS OPERACIONAIS II N (2016/1)
**
** Professor:
**     Alberto Egon Schaeffer Filho
**
**         COMUNICAÇÃO INTER-PROCESSOS USANDO SOCKETS UNIX
**
** Alunos:
**     Bruno Dias Freitas
**     Bruno Giovenardi Esteve
**     Erick Chemevski Espindula
**
** Descrição:
**
** Implementação do cliente do chat.
**
** Como usar: chamar ./client hostname
** hostname = localhost se for testado na mesma máquina com terminais diferentes
**
*****************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include "../include/terminal.h"
#include "../include/auxiliar.h"


#define BUFFER 256

typedef struct {
  boolean *p_continuar;
  char *msg_texto;
  char *nome_sala;
}ControleMsg;


int sockfd;
char nick[MAX_NICK] = NICK_PADRAO;
struct sockaddr_in serv_addr;
struct hostent *server;


// Envia mensagem ao servidor e aguarda uma resposta
int envia_msg_servidor(char *msg, int tam_msg, char *msg_ret, int tam_msg_ret, char *msg_erro)
{
    // Envia uma mensagem
    if(aux_envia_msg_socket(sockfd, msg, tam_msg) < 0)
    {
        terminal_mensagem_situacao(msg_erro);
        return 0;
    }
    else
    {
        // Aguarda a resposta
        if(aux_recebe_msg_socket(sockfd, msg_ret, tam_msg_ret) < 0)
        {
            terminal_mensagem_situacao(msg_erro);
            return 0;
        }
        return 1;
    }
}


// Recebe uma lista de salas que existem no servidor
void receber_salas(int *qtd_salas,int tam_sala, char salas[][tam_sala])
{
    *qtd_salas = 0;

    // As salas são recebidas uma por uma. Entao recebe a quantidade de salas a ser recebidas
    aux_recebe_msg_socket(sockfd, (char *) qtd_salas, sizeof(int));

    int i;
    // Recebe as salas
    for(i = 0; i < *qtd_salas; i++)
    {
        aux_recebe_msg_socket(sockfd, salas[i], MAX_NOME_SALA);
    }
    terminal_lista_salas(*qtd_salas, MAX_NOME_SALA, salas);
}


// Funcao usada na thread que trata as mensagens recebidas pelo servidor durante uma bate-papo
void *recebe_msg_chat(void *arg)
{
    ControleMsg *controle_msg = arg;
    char msg_recebida[MAX_MSG_TOTAL];
    int comando = 0;
    boolean continuar = TRUE;

    while(continuar)
    {
        // Recebe mensagem do servidor
        aux_recebe_msg_socket(sockfd, msg_recebida, MAX_MSG_TOTAL);

        // Verifica se usuario nao sai da sala
        comando = aux_compara_comandos(msg_recebida);
        if(comando != CMD_LEAVE_ID)
        {
            // Apenas escreve a mensagem caso usuario nao saiu da sala
            if(*controle_msg->p_continuar)
                // Escreve a mensagem no terminal
                terminal_escrever_mensagem_chat(msg_recebida, FALSE, controle_msg->msg_texto, controle_msg->nome_sala);
        }
        else
        {
            // Usuario saiu da sala, entao aborta o recebimento de novas mensagens
            continuar = FALSE;
        }
    }
    return NULL;
}


void inicia_chat(char *nome_sala)
{
    int bytes_lidos;
	char msg_texto[MAX_MSG_TEXTO];
	char msg_final[MAX_MSG_TOTAL];
    int comando = 0;

    boolean continua = TRUE;

    // Argumentos da função da thread que receberá as mensagens do servidor
    ControleMsg controle_msg;
    controle_msg.p_continuar = &continua;
    controle_msg.msg_texto = msg_texto;
    controle_msg.nome_sala = nome_sala;

    // Prepara a tela
    terminal_mensagem_inicial(nick);
    strcpy(msg_final, "<SISTEMA> Voce entrou na sala '");
    strcat(msg_final, nome_sala);
    strcat(msg_final, "'!");
    printf("%s\n", SEPARADOR_MSG);
    terminal_mensagem_padrao_chat(msg_final, nome_sala);

    // Cria a thread que será responsável por receber as mensagens do servidor
    pthread_t trecebe;
    pthread_create(&trecebe,NULL,recebe_msg_chat,(void *) &controle_msg);	// cria thread pra receber msg


    // A thread corrente será responsável por enviar mensagens para o serivodr
	while(continua){

		terminal_get_linha(msg_texto, MAX_MSG_TEXTO);
        comando = aux_compara_comandos(msg_texto);

        // Comando de sair da sala disparado
        if(comando == CMD_LEAVE_ID)
        {
            // Envia mensagem pro servidor informando que vai sair
            // Servidor ira responder para a thread de leitura para acabar execucao
            aux_envia_msg_socket(sockfd, msg_texto, MAX_MSG_TEXTO);
            continua = FALSE;

            // Aguar a thread encerrar para finalizar
            pthread_join(trecebe, NULL);
        }
        else
        {
            // Caso a mensagem nao seja vazio
            if(msg_texto[0] != '\0')
            {
                // Prepara a mensagem com o formato final
                strcpy(msg_final, nick);
                strcat(msg_final, " : ");
                strcat(msg_final, msg_texto);

                // Escreve no terminal
                terminal_escrever_mensagem_chat(msg_final, TRUE, NULL, nome_sala);

                // Envia mensagem pro servidor
                bytes_lidos = aux_envia_msg_socket(sockfd, msg_final, MAX_MSG_TOTAL);

                if (bytes_lidos < 0)
                    printf("ERRO! Erro ao enviar mensagem para o servidor!\n");
            }
            else
                printf("\033[1A"); // Sobe uma linha referente ao ENTER
        }
	}
}


int conectar(char *endereco) {

    // Verificar o hostname
	server = gethostbyname(endereco);
	if (server == NULL) {
        fprintf(stderr,"ERRO! Nao foi possivel conectar no host %s\n", endereco);
        return 0;
    }

    // Tenta obter um socket
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        	printf("ERRO! Nao foi possivel abrir o socket!\n");

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	serv_addr.sin_addr = *((struct in_addr *)server->h_addr);
	bzero(&(serv_addr.sin_zero), 8);

    // Tenta conectar usando o novo socket
	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){
	    printf("ERRO! Nao foi possivel conectar.\n");
		return 0;
	}

    // Verifica se o numero maximo de conexoes nao foi excedido
	char msg_ret[MAX_MSG_SERVIDOR];
	aux_recebe_msg_socket(sockfd, msg_ret, MAX_MSG_SERVIDOR);
	if(strcmp(msg_ret, STATUS_ERRO) == 0)
    {
        printf("ERRO! Numero maximo de conexoes excedido.\n");
		return 0;
    }

    return 1;
}


int main(int argc, char *argv[])
{

    if (argc < 2) {
		fprintf(stderr,"Argumentos invalidos. Informe o hostname: %s <hostname>\n", argv[0]);
		return 0;
    }

	if(!conectar(argv[1]))
        return 0;

    // Prepara as configuracoes iniciais do terminal
    if(!terminal_prepara_config())
        return 0;

    char msg_comando[MAX_COMANDO];
    char msg_servidor[MAX_MSG_SERVIDOR];
    char nick_novo[MAX_NICK];
    char salas[MAX_SALAS][MAX_NOME_SALA];
    char nome_sala[MAX_NOME_SALA];
    int qtd_salas;

    int comando = CMD_NICK_ID; // Primeiro o usuario deve escolher um nick
    boolean executa = TRUE;

	while(executa)
	{
        switch(comando)
        {

           // Sistema aguarda comando do usuario (Tela inicial)
            case NO_CMD:
                terminal_mensagem_inicial(nick);

                // Captura o codigo informado
                comando = terminal_escolher_comando(msg_comando, MAX_COMANDO);

                // Envia o codigo para o servidor
                if(!aux_envia_msg_socket(sockfd, msg_comando, MAX_COMANDO))
                    comando = NO_CMD;

                break;


            // Usuario escolhe um novo nick
            case CMD_NICK_ID:
                terminal_mensagem_inicial(nick);

                // Captura nick informado pelo cliente
                terminal_escolha_nick(nick_novo, MAX_NICK);

                // Envia novo nick para servido
                if(envia_msg_servidor(nick_novo, MAX_NICK, msg_servidor, MAX_MSG_SERVIDOR, "Erro ao atualizar o nick!"))
                {
                    // Imprime o retorno do servidor
                    terminal_mensagem_situacao(msg_servidor);
                    strcpy(nick, nick_novo);
                }
                comando = NO_CMD;
                break;


            // Criar uma nova sala
            case CMD_CREATE_ID:
                terminal_mensagem_inicial(nick);

                // Recebe uma lista de salas já existentes
                receber_salas(&qtd_salas, MAX_NOME_SALA, salas);

                // Captura nome da sala
                comando = terminal_criar_sala(nome_sala, MAX_NOME_SALA);

                if(comando != CMD_CANCEL_ID)
                {
                    // Envia nome da sala para o servidor
                    if(envia_msg_servidor(nome_sala, MAX_NOME_SALA, msg_servidor, MAX_MSG_SERVIDOR, "Erro ao tentar criar uma sala!"))
                    {
                        // Imprime o retorno do servidor
                        terminal_mensagem_situacao(msg_servidor);
                    }
                }
                else
                {
                    // Envia para o servidor que a operacao foi cancelada
                    aux_envia_msg_socket(sockfd, nome_sala, MAX_NOME_SALA);
                }
                comando = NO_CMD;
                break;


            // Entrar em uma sala
            case CMD_JOIN_ID:
                terminal_mensagem_inicial(nick);
                receber_salas(&qtd_salas, MAX_NOME_SALA, salas);

                // Captura nome da sala
                comando = terminal_entrar_sala(nome_sala, MAX_NOME_SALA, qtd_salas, salas);

                if(comando != CMD_CANCEL_ID)
                {
                    // Envia nome da sala para o servidor
                    if(envia_msg_servidor(nome_sala, MAX_NOME_SALA, msg_servidor, MAX_MSG_SERVIDOR, "Erro ao tentar entrar em uma sala!"))
                    {
                        if(strcmp(msg_servidor, STATUS_CONEC) == 0)
                        {
                            // Inicia o chat
                            inicia_chat(nome_sala);
                        }
                        else
                        {
                            // Informa o problema que ocorreu ao entrar na sala
                            terminal_mensagem_situacao(msg_servidor);
                        }
                    }
                }
                else
                {
                    // Envia para o servidor que a operacao foi cancelada
                    aux_envia_msg_socket(sockfd, nome_sala, MAX_NOME_SALA);
                }
                comando = NO_CMD;
                break;


            // Remove uma sala
            case CMD_REMOVE_ID:
                terminal_mensagem_inicial(nick);
                receber_salas(&qtd_salas, MAX_NOME_SALA, salas);

                // Captura nome da sala
                comando = terminal_remover_sala(nome_sala, MAX_NOME_SALA, qtd_salas, salas);

                if(comando != CMD_CANCEL_ID)
                {
                    // Envia nome da sala para o servidor
                    if(envia_msg_servidor(nome_sala, MAX_NOME_SALA, msg_servidor, MAX_MSG_SERVIDOR, "Erro ao tentar criar uma sala!"))
                    {
                        // Imprime o retorno do servidor
                        terminal_mensagem_situacao(msg_servidor);
                    }
                }
                else
                {
                    // Envia para o servidor que a operacao foi cancelada
                    aux_envia_msg_socket(sockfd, nome_sala, MAX_NOME_SALA);
                }
                comando = NO_CMD;
                break;


            // Encerrar o programa
            case CMD_EXIT_ID:
                executa = FALSE;
                break;
        }
	}

    terminal_reseta_config();
	close(sockfd);

    return 0;
}

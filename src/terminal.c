/****************************************************
** UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
** INSITUTO DE INFORMATICA
** INFO1151 - SISTEMAS OPERACIONAIS II N (2016/1)
**
** Professor:
**     Alberto Egon Schaeffer Filho
**
**         COMUNICAÇÃO INTER-PROCESSOS USANDO SOCKETS UNIX
**
** Alunos:
**     Bruno Dias Freitas
**     Bruno Giovenardi Esteve
**     Erick Chemevski Espindula
**
** Descrição:
**
** Implementação da biblioteca terminal
**
****************************************************/


#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <semaphore.h>
#include "../include/auxiliar.h"
#include "../include/terminal.h"

struct termios orig_term_attr;
struct termios new_term_attr;
sem_t mutex;


int terminal_prepara_config()
{
    tcgetattr(fileno(stdin), &orig_term_attr);
    memcpy(&new_term_attr, &orig_term_attr, sizeof(struct termios));
//    new_term_attr.c_lflag &= ~(ECHO|ICANON);
//    new_term_attr.c_lflag &= ~(ECHO);
    new_term_attr.c_lflag &= ~(ICANON);
    new_term_attr.c_cc[VTIME] = 0;
    new_term_attr.c_cc[VMIN] = 0;
    tcsetattr(fileno(stdin), TCSANOW, &new_term_attr);

    int res = sem_init(&mutex, 0, 1);
    if (res != 0) {
        printf("ERRO! Erro na inicializacao do semaforo!\n");
        return 0;
    }
    return 1;
}


void terminal_reseta_config()
{
    tcsetattr(fileno(stdin), TCSANOW, &orig_term_attr);
}


int terminal_get_tecla() {
    int tecla;

    // Recebe um caracter
    tecla = fgetc(stdin);

    return tecla;
}


void terminal_apaga_linha(char *c, int p)
{
    printf("\033[2K"); // Apaga linha
    printf("\033[1G"); // Move curso pra primeira posicao
    printf("%s\n", c);
    printf("\033[1A");
    printf("\033[%dG", p + 1);
}


void terminal_apaga_linhas_anteriores(int linhas)
{
    int i;
    for(i = 0; i < linhas; i++)
    {
        printf("\033[1A"); // Sobe uma linha
        printf("\033[2K"); // Apaga linha
    }
}


void terminal_get_linha(char *buffer, int tam)
{
    int i;
    // Limpa o buffer
    for(i = 0; i < tam; i++)
    {
        buffer[i] = '\0';
    }

    int tecla;
    i = 0;
    while(i < tam)
    {
        tecla = terminal_get_tecla();
        if(tecla != -1)
        {
            if(tecla == '\n')
                break;

            if(tecla == 127) // Backspace
            {
                if(i > 0)
                {
                    i--;
                    buffer[i] = '\0';
                    terminal_apaga_linha(buffer, i);
                }
                else
                {
                    printf("\033[2K"); // Apaga linha
                    printf("\033[1G"); // Move curso para a primeira posicao
                }
            }
            else if(tecla >= 32)
            {
                if(i == tam - 1) // Controla o limite máximo de caracteres digitados
                {
                    terminal_apaga_linha(buffer, i);
                }
                else
                {
                    buffer[i] = tecla;
                    i++;
                }
            }
        }
        usleep(1000); // Evita o uso excessivo do processador fazendo busy wait

    }
}


void terminal_mensagem_padrao_chat(char *mensagem, char *nome_sala)
{
    printf("\033[1G");
    printf("%s\n", mensagem); // Escreve a mensagem e desce uma linha
    printf("\n");
    printf("%s\n", SEPARADOR);
    printf("\n");
    printf("    Voce esta na sala '%s'!\n", nome_sala);
    printf("\n");
    printf("Dica: use o comando \\leave para sair da sala.\n");
    printf("\n");
    printf("Digite sua mensagem: \n");
}


void terminal_escrever_mensagem_chat(char *mensagem, int eh_usuario, char *buffer, char *nome_sala)
{
    sem_wait(&mutex);
    if(eh_usuario)
        printf("\033[1A"); // Sobe uma linha devido ao ENTER

    printf("\033[2K"); // Apaga linha corrente
    terminal_apaga_linhas_anteriores(8);

    terminal_mensagem_padrao_chat(mensagem, nome_sala);

    if(!eh_usuario)
    {
        if(buffer != '\0')
            printf("%s", buffer);
    }
    sem_post(&mutex);
}


void terminal_mensagem_inicial(char *nick)
{
    printf("\033[2J"); // Limpa o terminal
    printf("\033[1;1H");
    printf("%s\n", SEPARADOR);
    printf("%s\n", SEPARADOR_NOME);
    printf("%s\n\n", SEPARADOR);
    printf("Bem vindo ao %s, %s!\n\n", NOME_CHAT, nick);
}


void terminal_escolha_nick(char *nick, int tam)
{
    boolean nick_valido = FALSE;
    boolean primeira_vez = TRUE;

    int lin_head = 5; // Atualizar caso aumente o numero de prints abaixo
    printf("%s\n", SEPARADOR);
    printf("\n");
    printf("          ESCOLHA DE NICK\n");
    printf("\n");
    printf("OBS: o nome nao pode comecar com espaco ou '\\'.\n");

    while(!nick_valido)
    {
        if(!primeira_vez)
            printf("Nick invalido. ");

        primeira_vez = FALSE;

        printf("Por favor, digite o seu nick (maximo %d caracteres):\n", tam-1);
        terminal_get_linha(nick, tam);

        if(nick[0] != '\0' && nick[0] != ' ' && nick[0] != '\\')
            nick_valido = TRUE;

        terminal_apaga_linhas_anteriores(2);
    }
    terminal_apaga_linhas_anteriores(lin_head);
}


int terminal_escolher_comando(char *comando, int tam_max)
{
    boolean comando_valido = FALSE;
    boolean primeira_vez = TRUE;
    int cmd = 0;

    int lin_head = 11; // Atualizar caso aumente o numero de prints abaixo
    printf("%s\n", SEPARADOR);
    printf("\n");
    printf("          MENU INICIAL\n");
    printf("\n");
    printf("O que gostaria de fazer? Digite um dos comandos abaixo para executar uma acao.\n");
    printf("    %s - Entrar em uma sala\n", CMD_JOIN);
    printf("    %s - Criar uma nova sala\n", CMD_CREATE);
    printf("    %s - Excluir uma sala\n", CMD_REMOVE);
    printf("    %s - Alterar o nick\n", CMD_CHANGENICK);
    printf("    %s - Sair do programa\n", CMD_EXIT);
    printf("\n");

    while(!comando_valido)
    {
        if(!primeira_vez)
            printf("Comando invalido. ");

        primeira_vez = FALSE;

        printf("Digite um comando:\n");
        terminal_get_linha(comando, tam_max);

        if(comando[0] == '\\')
        {
            cmd = aux_compara_comandos(comando);
            switch(cmd)
            {
                case CMD_JOIN_ID:
                case CMD_CREATE_ID:
                case CMD_EXIT_ID:
                case CMD_NICK_ID:
                case CMD_REMOVE_ID:
                    comando_valido = TRUE;
                    break;
                default:
                    comando_valido = FALSE;
            }
        }
        terminal_apaga_linhas_anteriores(2);
    }

    terminal_apaga_linhas_anteriores(lin_head);
    return cmd;
}


void terminal_mensagem_situacao(char *msg)
{
    char buffer[1];

    printf("%s\n", msg);
    printf("Pressione ENTER para continuar.\n");
    terminal_get_linha(buffer, 1);

    terminal_apaga_linhas_anteriores(3);
}


void terminal_lista_salas(int qtd_salas, int tam_nome, char salas[qtd_salas][tam_nome])
{
    printf("%s\n", SEPARADOR);
    printf("Salas disponiveis:\n");

    if(qtd_salas < 1)
    {
        printf("   - Nenhuma sala encontrada!\n");
    }
    else
    {
        int i;
        for(i = 0; i < qtd_salas; i++)
        {
            printf("    '%s'\n", salas[i]);
        }
    }
    printf("\n");
}


int terminal_criar_sala(char *nome_sala, int tam_max)
{
    boolean nome_sala_valido = FALSE;
    boolean primeira_vez = TRUE;
    int cmd = 0;

    int lin_head = 9; // Atualizar caso aumente o numero de prints abaixo
    printf("%s\n", SEPARADOR);
    printf("\n");
    printf("          CRIACAO DE SALAS\n");
    printf("\n");
    printf("Para criar uma sala, digite o nome e pressione ENTER.\n");
    printf("OBS1: O nome da sala nao pode comecar com espaco ou com '\\'.\n");
    printf("OBS2: Digite o comando abaixo para cancelar a operacao.\n");
    printf("    \\cancel - Cancela a operacao\n");
    printf("\n");

    while(!nome_sala_valido)
    {
        if(!primeira_vez)
            printf("Nome ou comando invalido. ");

        primeira_vez = FALSE;

        printf("Digite o nome da sala:\n");
        terminal_get_linha(nome_sala, tam_max);

        if(nome_sala[0] == '\\')
        {
            cmd = aux_compara_comandos(nome_sala);
            switch(cmd)
            {
                case CMD_CANCEL_ID:
                    nome_sala_valido = TRUE;
                    break;
                default:
                    nome_sala_valido = FALSE;
            }
        }
        else if(nome_sala[0] != ' ' && nome_sala[0] != '\0' && nome_sala[0] != '\\')
        {
            nome_sala_valido = TRUE;
        }
        terminal_apaga_linhas_anteriores(2);
    }

    terminal_apaga_linhas_anteriores(lin_head);
    return cmd;
}


int terminal_entrar_sala(char *nome_sala, int tam_max, int nro_salas, char salas[][tam_max])
{
    boolean nome_sala_valido = FALSE;
    boolean sala_nao_existe = FALSE;
    boolean primeira_vez = TRUE;
    int cmd = 0;

    int lin_head = 9; // Atualizar caso aumente o numero de prints abaixo
    printf("%s\n", SEPARADOR);
    printf("\n");
    printf("          ENTRAR EM UMA SALA\n");
    printf("\n");
    printf("Para participar de uma sala, digite o nome da sala desejada e pressione ENTER.\n");
    printf("OBS1: Digite o nome da sala sem os apostrofos (') no inicio e no fim.\n");
    printf("OBS2: Digite o comando abaixo para cancelar a operacao.\n");
    printf("    \\cancel - Cancela a operacao\n");
    printf("\n");

    while(!nome_sala_valido)
    {
        if(sala_nao_existe)
            printf("Sala '%s' nao existe. ", nome_sala);
        else if(!primeira_vez)
            printf("Nome ou comando invalido. ");

        sala_nao_existe = FALSE;
        primeira_vez = FALSE;

        printf("Digite o nome da sala:\n");
        terminal_get_linha(nome_sala, tam_max);

        if(nome_sala[0] == '\\')
        {
            cmd = aux_compara_comandos(nome_sala);
            switch(cmd)
            {
                case CMD_CANCEL_ID:
                    nome_sala_valido = TRUE;
                    break;
                default:
                    nome_sala_valido = FALSE;
            }
        }
        else if(nome_sala[0] != ' ' && nome_sala[0] != '\0' && nome_sala[0] != '\\')
        {
            int i;
            for(i = 0; i < nro_salas; i++)
            {
                if(strcmp(nome_sala, salas[i]) == 0)
                {
                    nome_sala_valido = TRUE;
                    break;
                }
            }

            sala_nao_existe = TRUE;
        }

        terminal_apaga_linhas_anteriores(2);
    }

    terminal_apaga_linhas_anteriores(lin_head);
    return cmd;
}


int terminal_remover_sala(char *nome_sala, int tam_max, int nro_salas, char salas[][tam_max])
{
    boolean nome_sala_valido = FALSE;
    boolean sala_nao_existe = FALSE;
    boolean primeira_vez = TRUE;
    int cmd = 0;

    int lin_head = 9; // Atualizar caso aumente o numero de prints abaixo
    printf("%s\n", SEPARADOR);
    printf("\n");
    printf("          REMOVER UMA SALA\n");
    printf("\n");
    printf("Para remover uma sala, digite o nome da sala desejada e pressione ENTER.\n");
    printf("OBS1: Digite o nome da sala sem os apostrofos (') no inicio e no fim.\n");
    printf("OBS2: Digite o comando abaixo para cancelar a operacao.\n");
    printf("    \\cancel - Cancela a operacao\n");
    printf("\n");

    while(!nome_sala_valido)
    {
        if(nro_salas == 0)
            printf("Nao ha salas para serem excluidas. ");
        else if(sala_nao_existe)
            printf("Sala '%s' nao existe. ", nome_sala);
        else if(!primeira_vez)
            printf("Nome ou comando invalido. ");

        sala_nao_existe = FALSE;
        primeira_vez = FALSE;

        printf("Digite o nome da sala:\n");
        terminal_get_linha(nome_sala, tam_max);

        if(nome_sala[0] == '\\')
        {
            cmd = aux_compara_comandos(nome_sala);
            switch(cmd)
            {
                case CMD_CANCEL_ID:
                    nome_sala_valido = TRUE;
                    break;
                default:
                    nome_sala_valido = FALSE;
            }
        }
        else if(nro_salas > 0 && nome_sala[0] != ' ' && nome_sala[0] != '\0' && nome_sala[0] != '\\')
        {
            int i;
            for(i = 0; i < nro_salas; i++)
            {
                if(strcmp(nome_sala, salas[i]) == 0)
                {
                    nome_sala_valido = TRUE;
                    break;
                }
            }

            sala_nao_existe = TRUE;
        }
        terminal_apaga_linhas_anteriores(2);
    }

    terminal_apaga_linhas_anteriores(lin_head);
    return cmd;
}

#include <stdio.h>
#include <stdlib.h>
#include "../include/tabelaHash.h"


/*funcaoHash  recebe como parametro uma variavel do tipo inteiro(num),
retorna a  restra da divisao do valor dessa variavel pela tamanho da tabela*/
int funcaoHash(int num) {
    return (num % tam);
}

/*O procedimento inicializaHash recebe como parametro uma variavel do tipo
 Hash e sua funcao e que todas as posicoes da tab se tornem nulas*/
void inicializaHash(Hash tab) {
    int i;
    for (i = 0; i < tam; i++) {
        tab[i] = NULL;
    }
}

/*O procedimento insererHash recebe como parametro dois argumentos uma
variavel do tipo Hash e outra do tipo num. Sua funcao e inserir os elementos
na tabela atraveis da funcaoHash e caso esta posicao ja esteja preenchida,
como colisao esta sendo adotado neste procedimento o encadeamento direto.*/
void insereHash(Hash tab, int num, Sala sala) {
    int i = 0;
    int chave = funcaoHash(num);
    Dados* aux = tab[chave];
    while (aux != NULL) {
        if (aux->info == num) {
            break;
        }
        aux = aux->prox;
    }
    if (aux == NULL) {
        aux = (Dados*) malloc(sizeof (Dados));
        aux->sala = sala;
        aux->info = num;
        aux->prox = tab[chave];
        tab[chave] = aux;
    }
}

/*O procedimento buscaHash recebe como parametro duas variaveis uma do
tipo Hash(tab) e outra do tipo inteiro(num),A variavel tab tem como funcao
 passar a tabela e a variavel num tem como funcao determinar a posicao da
 tabela que o usuario deseja visualizar*/
Sala buscaHash(Hash tab, int num) {
    int pos = num;
    if (num > tam || num < 0) {
        printf("\nPosicao nao encontrada!");
        return;
    } else {
        Dados* aux = tab[pos];
        if (aux == NULL) {
            printf("Esta posicao esta vazia!");
            return;
        } else {
            return aux->sala;

        }
    }

    void imprimeHash(Hash tab) {
        int i = 0, cont = 0;
        for (i = 0; i < tam; i++) {
            if (tab[i] != NULL) {
                printf("\n %d", tab[i]->info);
                Dados* aux = tab[i]->prox;
                while (aux != NULL) {
                    printf(" -> %3d", aux->info);
                    aux = aux->prox;
                }
            }
        }
    }

    /*O procedimento removeHash recebe como parametro uma variavel do tipo Hash
    e outra do tipo inteiro, a variavel do tipo Hash serve para termos acesso
    a tabela e a variavel do tipo inteiro serve para escolher a posicao que o
    usuario deseja visualizar, apos a visualizacao da chave, o usuario escolhe a
    informacao da chave que deseja eliminar*/
    void removeHash(Hash tab, int num) {
        int pos = num;
        int ex;
        if (num > tam) {
            printf("\nEsta posicao nao existe na tabela!");
        } else {
            if (tab[pos] == NULL) {
                printf("Esta chave esta vazia!");
            } else {
                
                    if (tab[pos]->prox == NULL) {
                        tab[pos] = NULL;
                        return;
                    }
                    if (tab[pos]->prox != NULL) {
                        tab[pos]->sala = tab[pos]->prox->sala;
                        tab[pos]->info = tab[pos]->prox->info;
                        tab[pos]->prox = tab[pos]->prox->prox;
                        return;
                    }
                
            }
        }
    }
}
/****************************************************
** UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
** INSITUTO DE INFORMÁTICA
** INFO1151 - SISTEMAS OPERACIONAIS II N (2016/1)
**
** Professor:
**     Alberto Egon Schaeffer Filho
**
**         COMUNICAÇÃO INTER-PROCESSOS USANDO SOCKETS UNIX
**
** Alunos:
**     Bruno Dias Freitas
**     Bruno Giovenardi Esteve
**     Erick Chemevski Espindula
**
** Descrição:
**
** Implementação da biblioteca auxiliar
****************************************************/

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "../include/auxiliar.h"

//função pra tirar o '\n' no fim das strings
void aux_remove_quebra(char *msg_nova,char *msg_original)
{

	int i;
	for(i=0;msg_original[i]!='\n' && i < strlen(msg_original);i++)
		msg_nova[i] = msg_original[i];
}

int aux_compara_comandos(char *comando)
{
    if(strcmp(comando,CMD_LEAVE)==0)
		return CMD_LEAVE_ID;

	if(strcmp(comando,CMD_JOIN)==0)
		return CMD_JOIN_ID;

	if(strcmp(comando,CMD_CREATE)==0)
		return CMD_CREATE_ID;

	if(strcmp(comando,CMD_REMOVE)==0)
		return CMD_REMOVE_ID;

    if(strcmp(comando,CMD_CHANGENICK)==0)
		return CMD_NICK_ID;

    if(strcmp(comando,CMD_CANCEL)==0)
		return CMD_CANCEL_ID;

    if(strcmp(comando,CMD_EXIT)==0)
		return CMD_EXIT_ID;

	return NO_CMD;
}


int aux_envia_msg_socket(int socket_dest, char *msg, int tam_msg)
{
    int bytes_escr;
    char buffer[tam_msg];
    bzero(buffer, tam_msg);

    strcpy(buffer, msg);

    bytes_escr = write(socket_dest, buffer, tam_msg);
    return bytes_escr;
}


int aux_recebe_msg_socket(int socket_orig, char *msg, int tam_msg)
{
    int bytes_lidos;

    bzero(msg, tam_msg);
    bytes_lidos = read(socket_orig, msg, tam_msg);

    return bytes_lidos;
}


void aux_apaga_sala(Sala *sala)
{
    sala->id = 0;
    sala->nome[0] = '\0';
    sala->ocupado = FALSE;
}


void aux_inicia_salas(int qtd_sala, Sala sala[])
{
    int i;
    for(i = 0; i < qtd_sala; i++)
    {
        aux_apaga_sala(&sala[i]);
    }
}

void aux_apaga_cliente(Cliente *cliente)
{
    cliente->thread_id = 0;
    cliente->nick[0] = '\0';
    cliente->id = 0;
    cliente->socket = 0;
    cliente->sala = 0;
    cliente->sala = 0;
    cliente->sala = 0;
}

void aux_inicia_clientes(int qtd_cliente, Cliente clientes[])
{
    int i;
    for(i = 0; i < qtd_cliente; i++)
    {
        aux_apaga_cliente(&clientes[i]);
    }
}



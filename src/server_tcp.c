/****************************************************
** UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
** INSITUTO DE INFORMÁTICA
** INFO1151 - SISTEMAS OPERACIONAIS II N (2016/1)
**
** Professor:
**     Alberto Egon Schaeffer Filho
**
**         COMUNICAÇÃO INTER-PROCESSOS USANDO SOCKETS UNIX
**
** Alunos:
**     Bruno Dias Freitas
**     Bruno Giovenardi Esteve
**     Erick Chemevski Espindula
**
** Descrição:
**
** Implementação do cliente do chat.
**
*****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <semaphore.h>
#include "../include/auxiliar.h"


Sala lista_salas[MAX_SALAS];
Cliente lista_clientes[MAX_CLIENTS];
int nro_salas_ocupadas = 0;
int id_prox_cliente = 1;

sem_t mutex_sala;


//função para enviar mensagem a todos os clientes:
void enviarMSG(char *msg, Cliente *cliente) {
	int i ;

	for ( i = 0; i < MAX_CLIENTS; i++ ) {
		if ( lista_clientes[i].id > 0 && lista_clientes[i].id != cliente->id) {
			if( lista_clientes[i].sala == cliente->sala )
                aux_envia_msg_socket(lista_clientes[i].socket, msg, strlen(msg));
		}
	}
}

// Aloca novo cliente na lista de clientes, retorna -1 caso nao exita ou a posicao dele na lista de clintes
int aloca_cliente(int sock)
{
    int i;
    for(i = 0; i < MAX_CLIENTS; i++)
    {
        if(lista_clientes[i].id == 0)
        {
            lista_clientes[i].id = id_prox_cliente;
			lista_clientes[i].socket = sock;
			lista_clientes[i].sala = SALA_INICIAL;
			strcpy(lista_clientes[i].nick, NICK_PADRAO);
			id_prox_cliente++;
			return i;
        }
    }
    return -1;
}

// Busca por uma sala e retorna se ela id dela caso exita, ou -1 caso contrario
int encontra_sala(char *nome) {

	int i;
	for(i=0;i < MAX_SALAS;i++) {
        if(lista_salas[i].ocupado)
            if(strcmp(lista_salas[i].nome,nome) == 0) {
                return lista_salas[i].id;
		}
	}
	return -1;
}

// já recebeu o comando \nick e agora vai ler qual é o novo nick
void recebe_nick(Cliente *cliente) {
    int bytes_lidos;
	char novo_nick[MAX_NICK];

    bytes_lidos = aux_recebe_msg_socket(cliente->socket, novo_nick, MAX_NICK);
    if(bytes_lidos > 0)
    {
        bytes_lidos = aux_envia_msg_socket(cliente->socket, "Nick alterado com sucesso!", MAX_MSG_SERVIDOR);
        if(bytes_lidos > 0)
        {
            strcpy(cliente->nick,novo_nick);
            printf("Alterado nick do cliente de id %d para %s\n", cliente->id, cliente->nick);
            return;
        }
   }
   printf("ERRO! Erro ao alterar o nick do cliente de id %d\n", cliente->id);
}

// Tenta criar uma nova sala
void cria_sala(char *nome, char *msg_ret)
{
    if(nro_salas_ocupadas >= MAX_SALAS)
    {
        strcpy(msg_ret, "Nao foi possivel criar a sala! Numero maximo de salas ja atingido!");
        return;
    }

    boolean sala_exist = FALSE;
    int i;
    for(i = 0; i < MAX_SALAS; i++)
    {
        if(lista_salas[i].ocupado)
        {
            if(strcmp(lista_salas[i].nome, nome) == 0)
            {
                sala_exist = TRUE;
                break;
            }
        }
    }

    if(sala_exist)
    {
        strcpy(msg_ret, "Nao foi possivel criar a sala! Ja exite uma sala com este nome!");
        return;
    }

    for(i = 0; i < MAX_SALAS; i++)
    {
        if(!lista_salas[i].ocupado)
        {
            lista_salas[i].ocupado = TRUE;
            lista_salas[i].id = i+1;
            strcpy(lista_salas[i].nome, nome);
            printf("Criado a sala %s.\n", lista_salas[i].nome);
            nro_salas_ocupadas++;
            break;
        }
    }
    strcpy(msg_ret, "Sala criada com sucesso!");
}

boolean sala_sendo_usada(int id_sala)
{
    int i;
    for(i = 0; i < MAX_CLIENTS; i++)
    {
        if(lista_clientes[i].id > 0)
        {
            if(lista_clientes[i].sala == id_sala)
            {
                return TRUE;
            }
        }
    }
    return FALSE;
}

// Tenta remover uma sala
void remover_sala(char *nome, char *msg_ret)
{
    boolean sala_exist = FALSE;
    int i;

    // Mutex para evitar que sela seja fechada quando um usuario esteja entrando nela
    sem_wait(&mutex_sala);

    for(i = 0; i < MAX_SALAS; i++)
    {
        if(lista_salas[i].ocupado)
        {
            if(strcmp(lista_salas[i].nome, nome) == 0)
            {
                if(sala_sendo_usada(lista_salas[i].id))
                {
                    sem_post(&mutex_sala);
                    strcpy(msg_ret, "Nao foi possivel remover a sala! Ela esta sendo usada!");
                    return;
                }

                aux_apaga_sala(&lista_salas[i]);
                nro_salas_ocupadas--;
                sala_exist = TRUE;
                break;
            }
        }
    }

    sem_post(&mutex_sala);

    if(!sala_exist)
    {
        strcpy(msg_ret, "Nao foi possivel remover a sala! Sala nao existe!");
        return;
    }

    strcpy(msg_ret, "Sala removida com sucesso!");
}

// Envia lista de salas existentes para o cliente
void envia_lista_sala(Cliente *cliente)
{
    int qtd_salas = 0;
    char salas[MAX_SALAS][MAX_NOME_SALA];

    int i;
    for(i = 0; i < MAX_SALAS; i++)
    {
        if(lista_salas[i].ocupado)
        {
            strcpy(salas[qtd_salas], lista_salas[i].nome);
            qtd_salas++;
        }
    }

    if(aux_envia_msg_socket(cliente->socket, (char *) &qtd_salas, sizeof(int)) < 4)
    {
        printf("ERRO! Erro ao enviar o numero de salas para o cliente id %d", cliente->id);
    }
    else
    {
        for(i = 0; i < qtd_salas; i++)
        {
            aux_envia_msg_socket(cliente->socket, salas[i], MAX_NOME_SALA);
        }
    }

}

// Funcao para repassar as mensagens de um para os outros
void trata_chat_envio(Cliente *cliente) {

	boolean leave = FALSE;
	char msg_recebida[MAX_MSG_TOTAL];
	int comando;

    strcpy(msg_recebida, "<SISTEMA> ");
    strcat(msg_recebida, cliente->nick);
    strcat(msg_recebida, " entrou na sala.");
    enviarMSG(msg_recebida, cliente);

	while(!leave) {
        comando = NO_CMD;

        aux_recebe_msg_socket(cliente->socket, msg_recebida, MAX_MSG_TOTAL);

		if(msg_recebida[0]=='\\') {
            comando = aux_compara_comandos(msg_recebida);
            if(comando == CMD_LEAVE_ID)
            {
                // Envia uma mensagem de resposta para encerrar a thread que faz leitura
                aux_envia_msg_socket(cliente->socket, msg_recebida, MAX_MSG_TOTAL);
                leave = TRUE;
            }
		}
		else {
            // Envia mensagem para todos os outros clientes
			enviarMSG(msg_recebida, cliente);
		}
	}

    strcpy(msg_recebida, "<SISTEMA> ");
    strcat(msg_recebida, cliente->nick);
    strcat(msg_recebida, " saiu da sala.");
    enviarMSG(msg_recebida, cliente);

    printf("<SISTEMA> Cliente %d saiu da sala %d.\n", cliente->id, cliente->sala);
	cliente->sala = 0;

}

// recebeu o comando \join e agora decide que sala entrar
void entra_sala(Cliente *cliente, char *nome_sala, char *msg_ret) {

	int id_sala;

    // Semaforo para evitar que o usuario entre numa sala que está sendo fechada
    sem_wait(&mutex_sala);

    id_sala = encontra_sala(nome_sala);
	if(id_sala < 0) {
        sem_post(&mutex_sala);
		strcpy(msg_ret, "Sala nao encontrada!");
		aux_envia_msg_socket(cliente->socket, msg_ret, MAX_MSG_SERVIDOR);
	}
	else {
        strcpy(msg_ret, STATUS_CONEC);
        aux_envia_msg_socket(cliente->socket, msg_ret, MAX_MSG_SERVIDOR);
		cliente->sala = id_sala;
		sem_post(&mutex_sala);
		printf("Cliente %d entrou na sala %d.\n", cliente->id, cliente->sala);
		trata_chat_envio(cliente);
	}
}

// Função responsável por controlar as acoes do cliente
void *trata_cliente(void *arg) {

    Cliente *cliente = (Cliente *)arg;

	int comando;
	char msg_comando[MAX_COMANDO];
    char nome_sala[MAX_NOME_SALA];


	comando = CMD_NICK_ID; // Primeira acao do usuario sera atualizar o nick
	boolean exit = FALSE;
	while(!exit) {

		switch(comando) {

            // Aguardar Comando do ciente
            case NO_CMD:
                aux_recebe_msg_socket(cliente->socket, msg_comando, MAX_COMANDO);
                comando = aux_compara_comandos(msg_comando);
                break;


            // Entrar em uma sala
			case CMD_JOIN_ID:
				envia_lista_sala(cliente);

				aux_recebe_msg_socket(cliente->socket, nome_sala, MAX_NOME_SALA);

                comando = aux_compara_comandos(nome_sala);
                if(comando != CMD_CANCEL_ID)
				{
                    char msg_ret[MAX_MSG_SERVIDOR];
                    entra_sala(cliente, nome_sala, msg_ret);
                }
				comando = NO_CMD;

				break;


            // Criar uma sala
			case CMD_CREATE_ID:
				envia_lista_sala(cliente);

				aux_recebe_msg_socket(cliente->socket, nome_sala, MAX_NOME_SALA);

                comando = aux_compara_comandos(nome_sala);
                if(comando != CMD_CANCEL_ID)
				{
                    char msg_ret[MAX_MSG_SERVIDOR];
                    cria_sala(nome_sala, msg_ret);
                    aux_envia_msg_socket(cliente->socket, msg_ret, MAX_MSG_SERVIDOR);
                }
				comando = NO_CMD;
				break;


            // Remover uma sala
			case CMD_REMOVE_ID:
				envia_lista_sala(cliente);

				aux_recebe_msg_socket(cliente->socket, nome_sala, MAX_NOME_SALA);

                comando = aux_compara_comandos(nome_sala);
                if(comando != CMD_CANCEL_ID)
				{
                    char msg_ret[MAX_MSG_SERVIDOR];
                    remover_sala(nome_sala, msg_ret);
                    aux_envia_msg_socket(cliente->socket, msg_ret, MAX_MSG_SERVIDOR);
                }
				comando = NO_CMD;
				break;

            // Atualizar nick do cliente
			case CMD_NICK_ID:
				recebe_nick(cliente);
				comando = NO_CMD;
				break;


            // Encerrar conexão e liberar lista de cliente
			case CMD_EXIT_ID:
                close(cliente->socket);
                printf("Cliente %d saiu do chat.\n", cliente->id);
                aux_apaga_cliente(cliente);
				exit = TRUE;
				break;
		}
	}
	return NULL;
}


int main(int argc, char *argv[]) {

	int sockfd, newsockfd;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;
    int b = 1;

    printf("Iniciando o servidor...\n");

    int res = sem_init(&mutex_sala, 0, 1);
    if (res != 0) {
        printf("ERRO! Erro na inicializacao do semaforo!\n");
        return 0;
    }

	// primeira chamada pra socket()
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        printf("ERRO! Não foi possivel abrir o socket!\n");
		return 0;
	}


    if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&b,sizeof(int)) == -1) {
        close(sockfd);
        printf("ERRO! Tornar o socket reutilizavel!\n");
		return 0;
	}

	// inicializando a estrutura
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(serv_addr.sin_zero), 8);

	// bind do endereço
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
        printf("ERRO! Nao foi possivel realizar o bind do socket!\n A porta %d deve estar em uso!\n", PORT);
        printf("Se o erro persistir, por favor, alterar o numero da porta no arquivo auxiliar.h e recompilar.\n\n");
        close(sockfd);
        return 0;

    }

	if(listen(sockfd, 5) < 0)
	{
        printf("ERRO! Nao foi possivel criar o listen!\n");
        close(sockfd);
        return 0;
	}

    aux_inicia_salas(MAX_SALAS, lista_salas);
    aux_inicia_clientes(MAX_CLIENTS, lista_clientes);

	clilen = sizeof(struct sockaddr_in);


	printf("Servidor iniciado com sucesso!\n");

    while (TRUE) {
        // Aguarda nova solicitação de conexão
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

		if(newsockfd < 0) {
			perror("ERRO! Nao foi possivel estabelecer uma nova conexao!\n");
		}
		else{
			printf("Nova conexao estabelecida. Socket %d.\n", newsockfd);

            int sucesso = aloca_cliente(newsockfd);

            if(sucesso < 0)
            {
                // Envia mensagem de que está lotado
                aux_envia_msg_socket(newsockfd, STATUS_ERRO, MAX_MSG_SERVIDOR);
                close(newsockfd);
                printf("Cancelado conexao com socket %d. Numero maximo de clientes excedido\n", newsockfd);
            }
            else
            {
                // Cria nova thread para o nove cliente
                pthread_t new_thread;
                pthread_create(&new_thread,NULL,trata_cliente,(void *)&lista_clientes[sucesso]);
                printf("Criado cliente de id %d.\n", lista_clientes[sucesso].id);

                // Envia mensagem de que foi conectado
                aux_envia_msg_socket(newsockfd, STATUS_CONEC, MAX_MSG_SERVIDOR);
            }
			newsockfd = -1;
		}
	}

	close(sockfd);
	return 0;
}
